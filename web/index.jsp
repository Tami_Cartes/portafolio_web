
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Feria Virtual</title>

  <!-- Custom fonts for this template-->
   <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <!-- Lightbox-->
   <link rel="stylesheet" href="/Vendor/lightbox2/css/lightbox.min.css">
   <!-- Range slider-->
   <link rel="stylesheet" href="/Vendor/nouislider/nouislider.min.css">
   <!-- Bootstrap select-->
   <link rel="stylesheet" href="/Vendor/bootstrap-select/css/bootstrap-select.min.css">
   <!-- Owl Carousel-->
   <link rel="stylesheet" href="/Vendor/owl.carousel2/assets/owl.carousel.min.css">
   <link rel="stylesheet" href="/Vendor/owl.carousel2/assets/owl.theme.default.css">
   <!-- Google fonts-->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;400;700&amp;display=swap">
   <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Martel+Sans:wght@300;400;800&amp;display=swap">
   <!-- theme stylesheet-->
   <link rel="stylesheet" href="/css/style.default.css" id="theme-stylesheet">
   <!-- Custom stylesheet - for your changes-->
   <link rel="stylesheet" href="/css/custom.css">
   <!-- Favicon-->
   <link rel="shortcut icon" href="/img/favicon.png">

</head>

<body class="bg-gradient-primary">
    <style>
            body {
  background-color: #ffdd90;
  background-image: url("img/verduras.jpeg");
}
        </style>

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-lg-5">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row justify-content-center">
         
                <div class="col-md-9">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Bienvenido a la Feria Virtual</h1>
                  </div>
                    <form class="user" action="loginServlet" method="POST">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="rut" name="rut" placeholder="12345678-9" required>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="contrasena" name="contrasena" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="chbRecordar">
                        <label class="custom-control-label" for="chbRecordar">Mantener sesión</label>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      Iniciar sesión
                    </button>
                  </form>
                  <br>
                  <div class="text-center">
                    <a class="small" href="forgot-password.jsp">Olvidé mi contraseña</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="register.jsp">Registrarse</a>
                  </div>
                </div>
              
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
