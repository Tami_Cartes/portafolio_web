<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Maipo Grande</title>

        <!-- Custom fonts for this template-->
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Google fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;400;700&amp;display=swap">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Martel+Sans:wght@300;400;800&amp;display=swap">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="/css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="/css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="/img/favicon.png">

    </head>

    <body class="bg-gradient-primary">

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row justify-content-center">
                                <div class="col-md-11">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-4">Productos en subasta</h1>
                                        </div>
                                        <form class="user" method="POST" action="subastaServlet">
                                            <div class="form-group row">
                                                <div class="col-sm-6 mb-3 mb-sm-0">
                                                    <label>Precio</label>
                                                    <div id="">
                                                        <input type="number" id="precio" name="precio" class="form-control" value="" required>
                                                    </div>
                                                    <br>
                                                </div>
                                                <div class="col-sm-12">
                                                    <label>Descripción</label>
                                                </div>
                                                <div class="col-sm-12">        
                                                    
                                                    <textarea id="descripcion" name="descripcion" class="col-sm-12 form-control" type="text" required></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-6 mb-3 mb-sm-0">Tipo de envio</label>
                                                <div class="col-sm-12">
                                                    <select class="form-control form-control-user" name="tipo_envio" id="tipo_envio" required>
                                                        <option value="1">Nacional</option>
                                                        <option value="2">Internnacional</option>
                                                    </select>
                                                    <br>
                                                    <div>
                                                        <input type="file" id="archivo"  name="file" accept=".png, .jpg, .jpeg" multiple /><br/>
                                                    </div>
                                                    <br>
                                                </div>
                                                
                                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                                    Agregar Producto
                                                </button>
                                        </form>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="/vendor/jquery/jquery.min.js"></script>
        <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="/js/sb-admin-2.min.js"></script>

    </body>

</html>