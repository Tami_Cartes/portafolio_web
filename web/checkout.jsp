<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Feria Virtual</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css">
        <!-- Lightbox-->
        <link rel="stylesheet" href="/vendor/lightbox2/css/lightbox.min.css">
        <!-- Range slider-->
        <link rel="stylesheet" href="/vendor/nouislider/nouislider.min.css">
        <!-- Bootstrap select-->
        <link rel="stylesheet" href="/vendor/bootstrap-select/css/bootstrap-select.min.css">
        <!-- Owl Carousel-->
        <link rel="stylesheet" href="/vendor/owl.carousel2/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="/vendor/owl.carousel2/assets/owl.theme.default.css">
        <!-- Google fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;400;700&amp;display=swap">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Martel+Sans:wght@300;400;800&amp;display=swap">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="/css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="/css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="/img/favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
    <body>
        <style>
            body {
                background-color: #ffdd90;
                background-image: url("img/verduras.jpeg");
            }
        </style>
        <div class="page-holder">
            <!-- navbar-->
            <header class="header bg-white">
                <div class="container px-0 px-lg-3">
                    <nav class="navbar navbar-expand-lg navbar-light py-3 px-lg-0"><a class="navbar-brand" href=""><span class="font-weight-bold text-uppercase text-dark">Maipo Grande</span></a>
                        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto">               
                                <li class="nav-item"><a class="nav-link" href="#"> <i class="fas fa-user-alt mr-1 text-gray"></i>Login</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </header>
            <!--  Modal -->

            <div class="container">
                <!-- HERO SECTION-->
                <section class="py-5 bg-light">
                    <div class="container">
                        <div class="row px-4 px-lg-5 py-lg-4 align-items-center">
                            <div class="col-lg-6">
                                <h1 class="h2 text-uppercase mb-0">Confirmar Compra</h1>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="py-5 row d-flex column justify-content-center">
                    <!-- BILLING ADDRESS-->
                    <div class="card col-md-6">
                        <div class="card-body">
                            <h2 class="h5 text-uppercase mb-4">Informaci�n de env�o</h2>
                            <div class="row d-flex column justify-content-center">
                                <div class="d-flex column">
                                    <form method="POST" action="envioServlet">
                                        <div class="row "d-flex column">
                                            <div class="col-lg-6 form-group">
                                                <label class="text-small text-uppercase" for="rut">RUN</label>
                                                <input class="form-control form-control" id="rut" type="text">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label class="text-small text-uppercase" for="nombre">Nombre</label>
                                                <input class="form-control form-control" id="nombre" type="text">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label class="text-small text-uppercase" for="apaterno">Apellido Paterno </label>
                                                <input class="form-control form-control" id="apaterno" type="text">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label class="text-small text-uppercase" for="amaterno">Apellido Materno </label>
                                                <input class="form-control form-control" id="amaterno" type="text">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label class="text-small text-uppercase" for="email">Correo</label>
                                                <input class="form-control form-control" id="email" type="email">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label class="text-small text-uppercase" for="Telefono">Telefono</label>
                                                <input class="form-control form-control" id="telefono" type="number">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label class="text-small text-uppercase" for="ciudad">Ciudad</label>
                                                <input class="form-control form-control" id="ciudad" type="text">
                                            </div>
                                            <div class="col-lg-6 form-group">
                                                <label class="text-small text-uppercase" for="pais">Pa�s</label>
                                                <input type="text" class="form-control" name="pais" id="pais" required>
                                            </div>

                                            <div class="col-lg-6 form-group">
                                                <label class="text-small text-uppercase" for="region">Region</label>
                                                <input class="form-control form-control" id="region" type="text">
                                            </div>
                                            <div class="col-lg-12 form-group">
                                                <label class="text-small text-uppercase" for="direccion">Direcci�n</label>
                                                <input class="form-control form-control" id="direccion" type="text">
                                            </div>


                                        </div>
                                        <div class="col-lg-5 form-group">
                                            <button type="submit" class="btn btn-primary">Confirmar Compra</button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>

                </section>
            </div>
            <footer class="bg-dark text-white">

            </footer>
            <!-- JavaScript files-->
            <script src="vendor/jquery/jquery.min.js"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="vendor/lightbox2/js/lightbox.min.js"></script>
            <script src="vendor/nouislider/nouislider.min.js"></script>
            <script src="vendor/bootstrap-select/js/bootstrap-select.min.js"></script>
            <script src="vendor/owl.carousel2/owl.carousel.min.js"></script>
            <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.min.js"></script>
            <script src="js/front.js"></script>
            <script>
                // ------------------------------------------------------- //
                //   Inject SVG Sprite - 
                //   see more here 
                //   https://css-tricks.com/ajaxing-svg-sprite/
                // ------------------------------------------------------ //
                function injectSvgSprite(path) {

                    var ajax = new XMLHttpRequest();
                    ajax.open("GET", path, true);
                    ajax.send();
                    ajax.onload = function (e) {
                        var div = document.createElement("div");
                        div.className = 'd-none';
                        div.innerHTML = ajax.responseText;
                        document.body.insertBefore(div, document.body.childNodes[0]);
                    }
                }
                // this is set to BootstrapTemple website as you cannot 
                // inject local SVG sprite (using only 'icons/orion-svg-sprite.svg' path)
                // while using file:// protocol
                // pls don't forget to change to your domain :)
                injectSvgSprite('https://bootstraptemple.com/files/icons/orion-svg-sprite.svg');

            </script>
            <!-- FontAwesome CSS - loading as last, so it doesn't block rendering-->
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        </div>
    </body>
</html>