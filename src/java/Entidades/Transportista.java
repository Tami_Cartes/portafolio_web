/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author tamar
 */
public class Transportista {

    private int idTransp;
    private String rut;
    public Collection<Envio> envioCollection;

    public Transportista() {
    }

    public Transportista(Integer idTransp) {
        this.idTransp = idTransp;
    }

    public Integer getIdTransp() {
        return idTransp;
    }

    public void setIdTransp(Integer idTransp) {
        this.idTransp = idTransp;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

  


    @Override
    public String toString() {
        return "Entidades.Transportista[ idTransp=" + idTransp + " ]";
    }

}
