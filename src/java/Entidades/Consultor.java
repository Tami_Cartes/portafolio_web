/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tamar
 */

public class Consultor {

    private int idConsultor;
    private String rut;

    public Consultor() {
    }

    public Consultor(Integer idConsultor) {
        this.idConsultor = idConsultor;
    }

    public Consultor(Integer idConsultor, String rut) {
        this.idConsultor = idConsultor;
        this.rut =rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public Integer getIdConsultor() {
        return idConsultor;
    }

    public void setIdConsultor(Integer idConsultor) {
        this.idConsultor = idConsultor;
    }

    @Override
    public String toString() {
        return "Entidades.Consultor[ idConsultor=" + idConsultor + " ]";
    }
    
}
