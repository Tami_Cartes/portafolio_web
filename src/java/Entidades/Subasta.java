 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tamar
 */

public class Subasta {

    private int idSubasta;
    private int precio;
    private String descTransporte;
    private int idEnvio;
            
    public Subasta() {
    }

    public Subasta(Integer idSubasta) {
        this.idSubasta = idSubasta;
    }

    public Subasta(Integer idSubasta, int precio, String descTransporte) {
        this.idSubasta = idSubasta;
        this.precio = precio;
        this.descTransporte = descTransporte;
    }

    public Integer getIdSubasta() {
        return idSubasta;
    }

    public void setIdSubasta(Integer idSubasta) {
        this.idSubasta = idSubasta;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getDescTransporte() {
        return descTransporte;
    }

    public void setDescTransporte(String descTransporte) {
        this.descTransporte = descTransporte;
    }

    public int getIdEnvio() {
        return idEnvio;
    }

    public void setdEnvio(int idEnvio) {
        this.idEnvio = idEnvio;
    }

    @Override
    public String toString() {
        return "Subasta{ idSubasta=" + idSubasta + "precio= "+ precio+"descTransporte="+descTransporte+"idEnvio="+idEnvio+"}";
    }
    
}
