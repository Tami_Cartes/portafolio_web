/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tamar
 */

public class ResumenVenta {

    private int idResumen;
    private int costoTransporte;
    private int impuestos;
    private int pagoServicio;
    private int comisionEmpresa;
    private int idVenta;
    
    public ResumenVenta() {
    }

    public ResumenVenta(Integer idResumen) {
        this.idResumen = idResumen;
    }

    public ResumenVenta(Integer idResumen, int costoTransporte, int impuestos, int pagoServicio, int comisionEmpresa) {
        this.idResumen = idResumen;
        this.costoTransporte = costoTransporte;
        this.impuestos = impuestos;
        this.pagoServicio = pagoServicio;
        this.comisionEmpresa = comisionEmpresa;
    }

    public Integer getIdResumen() {
        return idResumen;
    }

    public void setIdResumen(Integer idResumen) {
        this.idResumen = idResumen;
    }

    public int getCostoTransporte() {
        return costoTransporte;
    }

    public void setCostoTransporte(int costoTransporte) {
        this.costoTransporte = costoTransporte;
    }

    public int getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(int impuestos) {
        this.impuestos = impuestos;
    }

    public int getPagoServicio() {
        return pagoServicio;
    }

    public void setPagoServicio(int pagoServicio) {
        this.pagoServicio = pagoServicio;
    }

    public int getComisionEmpresa() {
        return comisionEmpresa;
    }

    public void setComisionEmpresa(int comisionEmpresa) {
        this.comisionEmpresa = comisionEmpresa;
    }

    public int getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(int idVenta) {
        this.idVenta = idVenta;
    }

    @Override
    public String toString() {
        return "Entidades.ResumenVenta[ idResumen=" + idResumen + " ]";
    }
    
}
