/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import Entidades.Transportista;
import Conexion.ConexionBD;
import Entidades.Usuario;
import Interfaces.CRUDT;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author tamar
 */
public class DAOTransportista implements CRUDT{

    @Override
    public boolean crear(Transportista t, Usuario u) throws Exception {
       ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            
            DAOUsuario user = new DAOUsuario();
            user.crear(u);
            
            CallableStatement statement = conexion.getConexion().prepareCall("call insert_trans(?,?)");

            
            statement.setString(1, u.getRut());
            statement.registerOutParameter(2, OracleTypes.VARCHAR);
           
            statement.execute();
            statement.close();
            

        } catch (Exception e) {

            if (e.getMessage().substring(0, 9).equalsIgnoreCase("ORA-00001")) {
                throw new Exception("EL TRANSPORTISTA YA EXISTE");
            } else {
                throw new Exception("ERROR: " + e.getMessage());
            }
        }
        return false;
    }

    @Override
    public boolean eliminar(Transportista t, Usuario u) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {

            CallableStatement statement = conexion.getConexion().prepareCall("call delete_trans(?)");

            statement.setInt(1, t.getIdTransp());

            statement.execute();
            statement.close();

            DAOUsuario user = new DAOUsuario();

            user.eliminar(u);

        } catch (Exception ex) {
            throw new Exception("no se puede eliminar el transportista: " + ex.getMessage());

        }
        return false;
    }

    @Override
    public boolean modificar(Transportista t, Usuario u) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            DAOUsuario DAOUser = new DAOUsuario();
            CallableStatement statement = conexion.getConexion().prepareCall("call update_cliente(?, ?)");
            statement.setInt(1, t.getIdTransp());
            statement.setString(2, u.getRut());
            
            DAOUser.modificar(u);

            statement.execute();
            statement.close();

        } catch (Exception ex) {
            throw new Exception("No se puede editar el transportista: " + ex.getMessage());
        }

        return false;
    }

    public ArrayList<Transportista> listarTodo()throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            String query = "call listar_trans(?)";
            CallableStatement stmt = conexion.getConexion().prepareCall(query);

            stmt.registerOutParameter(1, OracleTypes.CURSOR);

            stmt.execute();

            ResultSet rs = (ResultSet) stmt.getObject(1);

            ArrayList<Transportista> LTr = new ArrayList();

            while (rs.next()) {
                Transportista trans = new Transportista();
                DAOUsuario user = new DAOUsuario();

                trans.setIdTransp(rs.getInt(1));
                trans.setRut(rs.getString(3));
                user.listarTodo();

                LTr.add(trans);

            }

            return LTr;

        } catch (Exception e) {
            throw new Exception("No se puede listar : " + e.getMessage());

        }
    }
 public Transportista buscarTransp(String rut) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();

        try {
            String query = "call buscar_transp(?)";
            CallableStatement stmt = conexion.getConexion().prepareCall(query);

            stmt.setString(1, rut);
            stmt.registerOutParameter(2, OracleTypes.NUMBER);

            stmt.execute();
            if(stmt.getString(2)==null){
            throw new Exception("el id buscada no existe");
            }
            
            Transportista recuperado = new Transportista();
            Usuario recuperado2 = new Usuario();
            
            recuperado2.setRut(rut);
            recuperado.setRut(rut);
            recuperado.setIdTransp(stmt.getInt(2));
            recuperado2.setNombre(stmt.getString(3));
            recuperado2.setApellidoPaterno(stmt.getString(4));
            recuperado2.setApellidoMaterno(stmt.getNString(5));
            recuperado2.setFechaNac(stmt.getDate(6));
            recuperado2.setCorreo(stmt.getString(7));
            recuperado2.setTipoUsuario(stmt.getString(8));
           
            
            conexion.getConexion().close();
            return recuperado;
        } catch (Exception e) {
            throw new Exception("No se puede obtener datos: " + e.getMessage());
        }

    }
}
