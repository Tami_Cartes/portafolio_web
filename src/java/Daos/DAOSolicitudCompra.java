/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import Entidades.SolicitudCompra;
import Conexion.ConexionBD;
import Interfaces.CRUD;
import java.security.cert.CRLSelector;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tamar
 */
public class DAOSolicitudCompra implements CRUD<SolicitudCompra> {

    private static String sql_selectAll = "select * from solicitud_compra";
    private static String sql_insert = "insert into solicitud_compra (id_solicitud) values(?)";
    private static String sql_delete = "delete from solicitud_compra where id_solicitud = ?";
    private static String sql_selectCliente = "select * from solicitud_compra where id_solicitud = ?";
    private static String sql_update = "update solicitud_compra where id_solicitud = ?";

    private static ConexionBD objConn;
    private ResultSet rs;

    @Override
    public boolean crear(SolicitudCompra o) {
        try {
            CallableStatement statement = objConn.getConexion().prepareCall("{inset_solicitud(?)}");

            statement.setInt(1, o.getIdSolicitud());

            statement.execute();
            statement.close();

            if (statement.executeUpdate() > 0) {
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DAOSolicitudCompra.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    @Override
    public boolean eliminar(SolicitudCompra o) {
        try {
           CallableStatement statement = objConn.getConexion().prepareCall("{delete_solicitud(?)}");

            statement.setInt(1, o.getIdSolicitud());

            statement.execute();
            statement.close();
            
            if (statement.executeUpdate() > 0) {
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DAOSolicitudCompra.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    public SolicitudCompra getConsultor(SolicitudCompra o) {
        try {
            SolicitudCompra solicomp = new SolicitudCompra();
            PreparedStatement psG;
            psG = objConn.getConexion().prepareStatement(sql_selectCliente);
            psG.setInt(1, o.getIdSolicitud());

            rs = psG.executeQuery();

            while (rs.next()) {
                solicomp.setIdSolicitud(rs.getInt("id_solicitud"));
            }

            return solicomp;
        } catch (SQLException ex) {
            Logger.getLogger(DAOSolicitudCompra.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    @Override
    public boolean modificar(SolicitudCompra o) {
        try {
             
            CallableStatement statement = objConn.getConexion().prepareCall("{update_solicitud(?)}");

            statement.setInt(1, o.getIdSolicitud());

            statement.execute();
            statement.close();

            if (statement.executeUpdate() > 0) {
                return true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(DAOSolicitudCompra.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return false;
    }

    @Override
    public ArrayList<SolicitudCompra> listarTodo() {
        try {
            ArrayList<SolicitudCompra> LSoliComp = new ArrayList();
            PreparedStatement ps;

            ps = objConn.getConexion().prepareStatement(sql_selectAll);
            rs = ps.executeQuery();
            while (rs.next()) {
                LSoliComp.add(new SolicitudCompra(rs.getInt("id_solicitud")));
            }

            objConn.getConexion().close();
            return LSoliComp;
        } catch (SQLException ex) {
            Logger.getLogger(DAOSolicitudCompra.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } 
    }

}
