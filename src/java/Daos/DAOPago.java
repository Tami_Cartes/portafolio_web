/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import Entidades.Pago;
import Conexion.ConexionBD;
import Entidades.Boleta;
import Interfaces.CRUDB;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author tamar
 */
public class DAOPago implements CRUDB {

    public boolean crear(Pago p, Boleta b) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {

            CallableStatement statement = conexion.getConexion().prepareCall("call insert_pago(?, ?,?,?)");

            statement.setInt(1, p.getIdPago());
            statement.setString(2, p.getTipoPago());
            statement.setInt(3, b.getIdBoleta());
            statement.registerOutParameter(4, OracleTypes.VARCHAR);
            
            statement.execute();
            statement.close();
            
            DAOBoleta bol = new DAOBoleta();
            bol.crear(b);
            
        } catch (Exception ex) {
            if (ex.getMessage().substring(0, 9).equalsIgnoreCase("ORA-00001")) {
                throw new Exception("EL Cliente YA EXISTE");
            } else {
                throw new Exception("ERROR: " + ex.getMessage());
            }
        }
        return false;
    }

    @Override
    public boolean eliminar(Pago p, Boleta b) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            CallableStatement statement = conexion.getConexion().prepareCall("call delete_pago(?)");

            statement.setInt(1, p.getIdPago());

            statement.execute();
            statement.close();
            
            DAOBoleta bol = new DAOBoleta();
            bol.eliminar(b);

        } catch (Exception ex) {
            throw new Exception("no se puede eliminar el pago: " + ex.getMessage());
        }
        return false;

    }

    @Override
    public boolean modificar(Pago p, Boleta b) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            CallableStatement statement = conexion.getConexion().prepareCall("call update_pago(?, ?,?)");

            statement.setInt(1, p.getIdPago());
            statement.setString(2, p.getTipoPago());
            statement.setInt(3, p.getIdBoleta());

            DAOBoleta bol = new DAOBoleta();
            bol.modificar(b);
            
            statement.execute();
            statement.close();

        } catch (Exception ex) {
            throw new Exception("No se puede editar el pago: " + ex.getMessage());
        }
        return false;

    }

    public ArrayList<Pago> listarTodo() throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            String query = "call listar_pago(?)";
            CallableStatement stmt = conexion.getConexion().prepareCall(query);

            stmt.registerOutParameter(1, OracleTypes.CURSOR);

            stmt.execute();

            ResultSet rs = (ResultSet) stmt.getObject(1);

            ArrayList<Pago> LPago = new ArrayList();

            while (rs.next()) {
                Pago pago = new Pago();
                DAOBoleta bol = new DAOBoleta();

                pago.setIdPago(rs.getInt(1));
                pago.setTipoPago(rs.getString(2));
                pago.setIdBoleta(rs.getInt(3));
                bol.listarTodo();

                LPago.add(pago);

            }

            return LPago;
        } catch (Exception e) {
            throw new Exception("No se puede listar : " + e.getMessage());

        }
    }

}
