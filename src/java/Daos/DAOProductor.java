/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import Entidades.Productor;
import Conexion.ConexionBD;
import Entidades.Usuario;
import Interfaces.CRUDPr;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author tamar
 */
public class DAOProductor implements CRUDPr {

    @Override
    public boolean crear(Usuario u,Productor p ) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            DAOUsuario user = new DAOUsuario();
            user.crear(u);
            
            CallableStatement statement = conexion.getConexion().prepareCall("call insert_pdctor(?,?,?)");

            statement.setInt(1, p.getIdProductor());
            statement.setString(2, u.getRut());
            statement.registerOutParameter(3, OracleTypes.VARCHAR);

            statement.execute();
            statement.close();

            

        } catch (Exception e) {

            if (e.getMessage().substring(0, 9).equalsIgnoreCase("ORA-00001")) {
                throw new Exception("EL PRODUCTOR YA EXISTE");
            } else {
                throw new Exception("ERROR: " + e.getMessage());
            }
        }
        return false;

    }

    @Override
    public boolean eliminar(Productor p, Usuario u) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            CallableStatement statement = conexion.getConexion().prepareCall("call delete_pdctor(?)");

            statement.setInt(1, p.getIdProductor());

            statement.execute();
            statement.close();

            DAOUsuario user = new DAOUsuario();
            user.eliminar(u);

        } catch (Exception ex) {
            throw new Exception("no se puede eliminar el cliente: " + ex.getMessage());

        }
        return false;
    }

    @Override
    public boolean modificar(Productor p, Usuario u) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            DAOUsuario DAOUser = new DAOUsuario();
            CallableStatement statement = conexion.getConexion().prepareCall("call update_pdctor(?, ?)");
            statement.setInt(1, p.getIdProductor());
            
            DAOUser.modificar(u);

            statement.execute();
            statement.close();

        } catch (Exception ex) {
            throw new Exception("No se puede editar el productor: " + ex.getMessage());
        }

        return false;
    }


    public ArrayList<Productor> listarTodo()throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            String query = "call listar_pdctor(?)";
            CallableStatement stmt = conexion.getConexion().prepareCall(query);

            stmt.registerOutParameter(1, OracleTypes.CURSOR);

            stmt.execute();

            ResultSet rs = (ResultSet) stmt.getObject(1);

            ArrayList<Productor> pr = new ArrayList();

            while (rs.next()) {
                Productor pro = new Productor();
                DAOUsuario user = new DAOUsuario();

                pro.setIdProductor(rs.getInt(1));
                pro.setRut(rs.getString(2));
                user.listarTodo();

                pr.add(pro);

            }

            return pr;

        } catch (Exception e) {
            throw new Exception("No se puede listar : " + e.getMessage());

        }
    }

}
