/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import Entidades.Consultor;
import Conexion.ConexionBD;
import Entidades.Usuario;
import Interfaces.CRUDCon;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.internal.OracleTypes;
import oracle.jdbc.oracore.OracleType;

/**
 *
 * @author tamar
 */
public class DAOConsultor implements CRUDCon{

    private static ConexionBD objConn;
    private ResultSet rs;

    @Override
    public boolean crear(Consultor c, Usuario u) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {

            CallableStatement statement = conexion.getConexion().prepareCall("call insert_consultor(?,?)");

            statement.setInt(1, c.getIdConsultor());
            statement.registerOutParameter(2, OracleTypes.VARCHAR);

            statement.execute();
            statement.close();

            DAOUsuario user = new DAOUsuario();
            user.crear(u);
            
            
        } catch (Exception e) {

            if (e.getMessage().substring(0, 9).equalsIgnoreCase("ORA-00001")) {
                throw new Exception("EL USUARIO YA EXISTE");
            } else {
                throw new Exception("ERROR: " + e.getMessage());
            }
        }
        return false;
    }

    @Override
    public boolean eliminar(Consultor c, Usuario u)throws Exception {
         ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {

            CallableStatement statement = conexion.getConexion().prepareCall("call delete_cliente(?)");

            statement.setInt(1, c.getIdConsultor());

            statement.execute();
            statement.close();

            DAOUsuario user = new DAOUsuario();

            user.eliminar(u);

        } catch (Exception ex) {
            throw new Exception("no se puede eliminar el cliente: " + ex.getMessage());

        }
        return false;
    }

    public boolean modificar(Consultor c, Usuario u) throws Exception{
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            DAOUsuario DAOUser = new DAOUsuario();
            CallableStatement statement = conexion.getConexion().prepareCall("call update_cliente(?, ?)");
            statement.setInt(1, c.getIdConsultor());
            
            DAOUser.modificar(u);

            statement.execute();
            statement.close();

        } catch (Exception ex) {
            throw new Exception("No se puede editar el cliente: " + ex.getMessage());
        }

        return false;
    }

    public ArrayList<Consultor> listarTodo() throws Exception{
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            String query = "call listar_cliente(?)";
            CallableStatement stmt = conexion.getConexion().prepareCall(query);

            stmt.registerOutParameter(1, OracleTypes.CURSOR);

            stmt.execute();

            ResultSet rs = (ResultSet) stmt.getObject(1);

            ArrayList<Consultor> LCon = new ArrayList();

            while (rs.next()) {
                Consultor con = new Consultor();
                DAOUsuario user = new DAOUsuario();

                con.setIdConsultor(rs.getInt(1));
                con.setRut(rs.getString(3));
                user.listarTodo();

                LCon.add(con);

            }

            return LCon;

        } catch (Exception e) {
            throw new Exception("No se puede listar : " + e.getMessage());

        }
    }

}
