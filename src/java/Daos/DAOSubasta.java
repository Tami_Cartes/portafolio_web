/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import Entidades.Subasta;
import Conexion.ConexionBD;
import Entidades.Envio;
import Entidades.Transportista;
import Interfaces.CRUDS;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author tamar
 */
public class DAOSubasta implements CRUDS {

    @Override
    public boolean crear(Subasta s, Envio e) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            CallableStatement statement = conexion.getConexion().prepareCall("call insert_subasta(?,?,?,?)");

            DAOEnvio en = new DAOEnvio();
            DAOTransportista daot = new DAOTransportista();
            
            statement.setInt(1, s.getPrecio());
            statement.setString(2, s.getDescTransporte());
            statement.setInt(3, e.getIdEnvio());
            statement.registerOutParameter(4, OracleTypes.VARCHAR);

            statement.execute();
            statement.close();

        } catch (Exception ex) {

            if (ex.getMessage().substring(0, 9).equalsIgnoreCase("ORA-00001")) {
                throw new Exception("LA SUBASTA YA EXISTE");
            } else {
                throw new Exception("ERROR: " + ex.getMessage());
            }
        }
        return false;
    }

    @Override
    public boolean eliminar(Subasta s, Envio e) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {

            CallableStatement statement = conexion.getConexion().prepareCall("call delete_subasta(?)");

            statement.setInt(1, s.getIdEnvio());

            statement.execute();
            statement.close();

            DAOEnvio en = new DAOEnvio();
            DAOTransportista daot = new DAOTransportista();

            Transportista t = new Transportista();
            daot.buscarTransp(t.getRut());

            en.eliminar(e, t);

        } catch (Exception ex) {
            throw new Exception("no se puede eliminar la Subasta: " + ex.getMessage());

        }
        return false;
    }

    public boolean modificar(Subasta s, Envio e) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            DAOEnvio en = new DAOEnvio();
            CallableStatement statement = conexion.getConexion().prepareCall("call update_cliente(?, ?)");
            statement.setInt(1, s.getIdEnvio());
            statement.setInt(2, s.getPrecio());
            statement.setString(3, s.getDescTransporte());
            statement.setInt(4, e.getIdEnvio());

            DAOTransportista daot = new DAOTransportista();

            Transportista t = new Transportista();
            daot.buscarTransp(t.getRut());

            en.modificar(e, t);

            statement.execute();
            statement.close();

        } catch (Exception ex) {
            throw new Exception("No se puede editar el Subasta: " + ex.getMessage());
        }

        return false;
    }

    public ArrayList<Subasta> listarTodo() throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            String query = "call listar_subasta(?)";
            CallableStatement stmt = conexion.getConexion().prepareCall(query);

            stmt.registerOutParameter(1, OracleTypes.CURSOR);

            stmt.execute();

            ResultSet rs = (ResultSet) stmt.getObject(1);

            ArrayList<Subasta> LSub = new ArrayList();

            while (rs.next()) {
                Subasta su = new Subasta();
                Envio e = new Envio();
                DAOEnvio en = new DAOEnvio();

                su.setIdSubasta(rs.getInt(1));
                su.setPrecio(rs.getInt(2));
                su.setDescTransporte(rs.getString(3));
                e.setTipoEnvio(rs.getString(4));
                en.listarTodo();
                LSub.add(su);
          
               

            }

            return LSub;

        } catch (Exception e) {
            throw new Exception("No se puede listar : " + e.getMessage());

        }

    }

}
