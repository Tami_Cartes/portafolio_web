/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import Entidades.Envio;
import Conexion.ConexionBD;
import Entidades.Transportista;
import Entidades.Usuario;
import Interfaces.CRUDEn;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author tamar
 */
public class DAOEnvio implements CRUDEn {

    private static ConexionBD objConn;
    private ResultSet rs;

    @Override
    public int crear(Envio e, Transportista t) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {

            CallableStatement statement = conexion.getConexion().prepareCall("call insert_envio(?,?,?,?,?,?)");
            DAOUsuario user = new DAOUsuario();
            DAOTransportista transp = new DAOTransportista();
            
           Transportista tr =transp.buscarTransp(t.getRut());
            
            statement.setString(1, e.getDireccion());
            statement.setString(2, e.getComuna());
            statement.setString(3, e.getRegion());
            statement.setString(4, e.getTipoEnvio());
            statement.setInt(5, tr.getIdTransp());
            statement.registerOutParameter(6, OracleTypes.VARCHAR);           
            
            statement.execute();
            statement.close();
            
            return statement.getInt(3);
            
        } catch (Exception ex) {

            if (ex.getMessage().substring(0, 9).equalsIgnoreCase("ORA-00001")) {
                throw new Exception("El ENVIO YA EXISTE");
            } else {
                throw new Exception("ERROR: " + ex.getMessage());
            }
        }
    

    }

    @Override
    public boolean eliminar(Envio e, Transportista t) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {

            CallableStatement statement = conexion.getConexion().prepareCall("call delete_envio(?)");

            statement.setInt(1, e.getIdEnvio());

            statement.execute();
            statement.close();

            DAOTransportista trans = new DAOTransportista();
            DAOUsuario du = new DAOUsuario();
            
            Usuario u = du.buscarUsuario(t.getRut());
            trans.eliminar(t,u);

        } catch (Exception ex) {
            throw new Exception("no se puede eliminar el envio: " + ex.getMessage());
        }
        return false;

    }

    @Override
    public boolean modificar(Envio e, Transportista t) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();
        try {
            DAOTransportista trans = new DAOTransportista();
            CallableStatement statement = conexion.getConexion().prepareCall("call update_envio(?, ?,?,?)");

            statement.setString(1, e.getDireccion());
            statement.setString(2, e.getComuna());
            statement.setString(3, e.getRegion());
            statement.setString(4, e.getTipoEnvio());
      

            statement.execute();
            statement.close();

            DAOUsuario du = new DAOUsuario();
            
            Usuario u = du.buscarUsuario(t.getRut());
            trans.modificar(t,u);
        } catch (Exception ex) {
            throw new Exception("No se puede editar el envio: " + ex.getMessage());
        }

        return false;
    }

    public ArrayList<Envio> listarTodo() throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();

        try {
            String query = "call listar_envio(?)";
            CallableStatement stmt = conexion.getConexion().prepareCall(query);

            stmt.registerOutParameter(1, OracleTypes.CURSOR);

            stmt.execute();

            ResultSet rs = (ResultSet) stmt.getObject(1);

            ArrayList<Envio> LEnvio = new ArrayList();

            while (rs.next()) {
                Envio e = new Envio();
                DAOTransportista tr = new DAOTransportista();

                e.setIdEnvio(rs.getInt(1));
                e.setDireccion(rs.getString(2));
                e.setComuna(rs.getString(3));
                e.setRegion(rs.getString(4));
                e.setTipoEnvio(rs.getString(5));
                e.setIdTransp(rs.getInt(6));
                tr.listarTodo();

                LEnvio.add(e);
            }
            return LEnvio;
        } catch (Exception e) {
            throw new Exception("No se puede listar : " + e.getMessage());

        }
    }
     public Envio buscarEnvio(int id) throws Exception {
        ConexionBD conexion = new ConexionBD();
        conexion.Conectar();

        try {
            String query = "call buscar_envio(?,?)";
            CallableStatement stmt = conexion.getConexion().prepareCall(query);

            stmt.setInt(1, id);
            stmt.registerOutParameter(2, OracleTypes.NUMBER);

            stmt.execute();
            if(stmt.getString(2)==null){
            throw new Exception("el id buscada no existe");
            }
            
            Envio recuperado = new Envio();

            recuperado.setIdTransp(id);
            recuperado.setIdEnvio(stmt.getInt(2));
            recuperado.setTipoEnvio(stmt.getString(3));
            
           
            
            conexion.getConexion().close();
            return recuperado;
        } catch (Exception e) {
            throw new Exception("No se puede obtener datos: " + e.getMessage());
        }

    }
}
