/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.Cliente;
import Entidades.Usuario;
import java.util.ArrayList;
import sun.net.www.content.text.Generic;

/**
 *
 * @author tamar
 */
public interface CRUDCli {
    public boolean crear(Cliente c, Usuario u) throws Exception;
    public boolean eliminar(Cliente c, Usuario u)throws Exception;
    public boolean modificar(Cliente c, Usuario u)throws Exception;
    
}
