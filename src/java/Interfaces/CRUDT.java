/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.Transportista;
import Entidades.Usuario;

/**
 *
 * @author tamar
 */
public interface CRUDT {
    public boolean crear(Transportista t, Usuario u) throws Exception;
    public boolean eliminar(Transportista t, Usuario u)throws Exception;
    public boolean modificar(Transportista t, Usuario u)throws Exception;
}
