/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.Envio;
import Entidades.Subasta;

/**
 *
 * @author tamar
 */
public interface CRUDS {
    public boolean crear(Subasta s, Envio e ) throws Exception;

    public boolean eliminar(Subasta s, Envio e ) throws Exception;

    public boolean modificar(Subasta s, Envio e ) throws Exception;
}
