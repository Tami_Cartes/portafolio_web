/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.Productor;
import Entidades.Usuario;

/**
 *
 * @author tamar
 */
public interface CRUDPr {
    public boolean crear(Usuario u,Productor p ) throws Exception;
    public boolean eliminar(Productor p, Usuario u)throws Exception;
    public boolean modificar(Productor p, Usuario u)throws Exception;
}
