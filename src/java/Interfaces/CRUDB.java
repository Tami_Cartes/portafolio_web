/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.Boleta;
import Entidades.Pago;

/**
 *
 * @author tamar
 */
public interface CRUDB {
    public boolean crear(Pago p, Boleta b) throws Exception;

    public boolean eliminar(Pago p, Boleta b) throws Exception;

    public boolean modificar(Pago p, Boleta b) throws Exception;
}
