/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.Producto;
import Entidades.Productor;

/**
 *
 * @author tamar
 */
public interface CRUDPro {
     public boolean crear(Producto pr, Productor p) throws Exception;
    public boolean eliminar(Producto pr, Productor p)throws Exception;
    public boolean modificar(Producto pr, Productor p)throws Exception;
    
}
