/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.Contrato;
import Entidades.Productor;

/**
 *
 * @author tamar
 */
public interface CRUDCont {
     public boolean crear(Contrato c, Productor p) throws Exception;
    public boolean eliminar(Contrato c, Productor p)throws Exception;
    public boolean modificar(Contrato c, Productor p)throws Exception;
}
