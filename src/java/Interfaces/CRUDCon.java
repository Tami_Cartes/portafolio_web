/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.Consultor;
import Entidades.Usuario;

/**
 *
 * @author tamar
 */
public interface CRUDCon {
    public boolean crear(Consultor c, Usuario u) throws Exception;
    public boolean eliminar(Consultor c, Usuario u)throws Exception;
    public boolean modificar(Consultor c, Usuario u)throws Exception;
}
