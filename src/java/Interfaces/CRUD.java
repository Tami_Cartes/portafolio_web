/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.Usuario;
import java.util.ArrayList;

/**
 *
 * @author tamar
 * @param <Generic>
 */
public interface CRUD <Generic>{
    public boolean crear(Generic o) throws Exception;
    public boolean eliminar(Generic o)throws Exception;
    public boolean modificar(Generic o)throws Exception;
    public ArrayList<Generic> listarTodo() throws Exception;
    
    
}

