/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.Envio;
import Entidades.Transportista;

/**
 *
 * @author tamar
 */
public interface CRUDEn {

    public int crear(Envio e, Transportista t) throws Exception;

    public boolean eliminar(Envio e, Transportista t) throws Exception;

    public boolean modificar(Envio e, Transportista t) throws Exception;
}
