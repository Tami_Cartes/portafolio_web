/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.ResumenVenta;
import Entidades.Venta;

/**
 *
 * @author tamar
 */
public interface CRUDResumen {
    public boolean crear(ResumenVenta rv, Venta v) throws Exception;

    public boolean eliminar(ResumenVenta rv, Venta v) throws Exception;

    public boolean modificar(ResumenVenta rv, Venta v) throws Exception;
}
