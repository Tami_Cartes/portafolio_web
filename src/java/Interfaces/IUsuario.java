/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Entidades.Cliente;
import Entidades.Usuario;

/**
 *
 * @author tamar
 */
public interface IUsuario {
     int crearUsuario(Usuario usuario, int rut );
     Usuario buscarUsuario( String rut,String clave );
     Cliente datosCliente (int idCli);
     boolean modificarUsuario (Usuario usuario, int rut);
}
