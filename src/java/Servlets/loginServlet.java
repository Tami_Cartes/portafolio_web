/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DAOS.*;
import Entidades.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tamara
 */
@WebServlet(name = "loginServlet", urlPatterns = {"/loginServlet"})
public class loginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        response.setContentType("text/html;charset=UTF-8");
        String rut = request.getParameter("rut");
        String contrasena = request.getParameter("contrasena");
  //      IUsuario usuarioDAO = null;
        DAOUsuario daou = new DAOUsuario();
        DAOProducto daopr = new DAOProducto();
        DAOSubasta daosub = new DAOSubasta();
        ArrayList<Producto> listaProductos = new ArrayList<Producto>();
        ArrayList<Subasta> listaSubasta = new ArrayList<Subasta>();
        
        try {
            listaProductos = daopr.listarTodo();
            listaSubasta = daosub.listarTodo();
            
        } catch (Exception ex) {
            Logger.getLogger(loginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       // DAOCliente daocli = new DAOCliente();
        
        Usuario user;
        //Cliente cliente = new Cliente();
        try {
   //         usuarioDAO = new UsuarioImplementacion();

            user = daou.buscarUsuario(rut);
            
            //cliente =daocli.buscarCliente(user.getRut()); buscar cliente
            if (user.getContrasena().equalsIgnoreCase(contrasena)) {
                if (user != null) {
                
               
                session.setAttribute("rut", user.getRut());
                session.setAttribute("nombreUser", user.getNombre());
                session.setAttribute("apaternoUser", user.getApellidoPaterno());
                session.setAttribute("amaternoUser", user.getApellidoMaterno());
                session.setAttribute("fecha_nac", user.getFechaNac());
                session.setAttribute("correo", user.getCorreo());
                session.setAttribute("contrasena", user.getContrasena());
                if(user.getTipoUsuario().equals("cliente")){
                   response.sendRedirect("productos.jsp");
                   session.setAttribute("listaProductos", listaProductos );
                }else if(user.getTipoUsuario().equals("productor")){
                    response.sendRedirect("index_pdctor.jsp");
                    session.setAttribute("listaProductos", listaProductos );
                }else if(user.getTipoUsuario().equals("transportista")){
                    response.sendRedirect("index_transp.jsp");
                    session.setAttribute("listaSubasta", listaSubasta);
                }else if(user.getTipoUsuario().equals("consultor")){
                    
                }
                 
                
        //       session.setAttribute("TipoCliente", cliente.getTipoCliente());
        //        if (cliente.getTipoCliente() == null) {
        //          cliente.setTipoCliente("Externo");
        //    }
        //        session.setAttribute("idCliente", cliente.getIdCliente());
        //        session.setAttribute("tipo_usuario", cliente.getTipoCliente());

                
            } else {
                out.println("<script src='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.all.js'></script>");
                out.println("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>");
                out.println("<script>");
                out.println("$(document).ready(function(){");
                out.println("swal('No se pudo iniciar sesion', 'Usuario o clave incorrecto', 'error');");
                out.println("});");
                out.println("</script>");
                RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
                rd.include(request, response);
            }
            }else{
                out.println("<script src='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.all.js'></script>");
                out.println("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>");
                out.println("<script>");
                out.println("$(document).ready(function(){");
                out.println("swal('No se pudo iniciar sesion', 'Usuario o clave incorrecto', 'error');");
                out.println("});");
                out.println("</script>");
                RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
                rd.include(request, response);
            }
            
        } catch (Exception ex) {
            Logger.getLogger(loginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
