/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DAOS.DAOEnvio;
import DAOS.DAOTransportista;
import Entidades.Envio;
import Entidades.Transportista;
import Entidades.Usuario;
import com.sun.xml.ws.runtime.dev.Session;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tamar
 */
@WebServlet(name = "envioServlet", urlPatterns = {"/envioServlet"})
public class envioServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            HttpSession session = request.getSession();
            Envio e = new Envio();
           
            DAOEnvio daoen = new DAOEnvio();
            DAOTransportista t = new DAOTransportista();
            Transportista tr =new Transportista();
            String rut = request.getParameter("rut");
            String nombre = request.getParameter("nombre");
            String apaterno = request.getParameter("apaterno");
            String amaterno = request.getParameter("amaterno");
            String correo = request.getParameter("email");
            String ciudad = request.getParameter("pais");
            String region = request.getParameter("region");
            String comuna = request.getParameter("comuna");
            String direccion = request.getParameter("direccion");
            
          
            session.setAttribute("rut", rut);
            tr = t.buscarTransp(rut);
            System.out.println(rut);
            e.setDireccion(direccion);
            e.setComuna(comuna);
            e.setRegion(region);
            e.setIdTransp(tr.getIdTransp());
            
            
            daoen.crear(e, tr);
            
            response.sendRedirect("compra.jsp");
        } catch (Exception ex) {
            Logger.getLogger(envioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
