/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DAOS.DAOProducto;
import Entidades.Producto;
import Entidades.Productor;
import com.sun.xml.rpc.processor.modeler.j2ee.xml.descriptionType;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tamar
 */
@WebServlet(name = "pdctoServerlet", urlPatterns = {"/pdctoServerlet"})
public class pdctoServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            
            DAOProducto daopr = new DAOProducto();
            ArrayList<Producto> lista = new ArrayList<>();
            lista = daopr.listarTodo();

            for (Producto producto : lista) {
                System.out.println(producto.toString());
            }
        } catch (Exception ex) {
            Logger.getLogger(pdctoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();

        response.setContentType("text/html;charset=UTF-8");
        try {

            String nomPdcto = request.getParameter("nomPdcto");
            int precio = Integer.parseInt(request.getParameter("precio"));
            int cantidad = Integer.parseInt(request.getParameter("cantidad"));
            String descripcion = request.getParameter("descripcion");

            Producto pro = new Producto();
            Productor pr = new Productor();
            DAOProducto daopr = new DAOProducto();

            pro.setNombre(nomPdcto);
            pro.setPrecio(precio);
            pro.setCantidad(cantidad);
            pro.setDescripcion(descripcion);
            pro.setIdProductor(pr.getIdProductor());

            daopr.crear(pro, pr);

            response.sendRedirect("productos.jsp");

        } catch (Exception ex) {
            Logger.getLogger(pdctoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
