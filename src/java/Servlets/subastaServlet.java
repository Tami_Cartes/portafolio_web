/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DAOS.DAOEnvio;
import DAOS.DAOSubasta;
import DAOS.DAOTransportista;
import Entidades.Envio;
import Entidades.Subasta;
import Entidades.Transportista;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tamar
 */
@WebServlet(name = "subastaServlet", urlPatterns = {"/subastaServlet"})
public class subastaServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            DAOSubasta daosub = new DAOSubasta();
            DAOEnvio daoenvio = new DAOEnvio();

            Subasta sub = new Subasta();
            Envio en = new Envio();

            ArrayList<Subasta> lista = new ArrayList<>();
            lista = daosub.listarTodo();

            for (Subasta subasta : lista) {
                System.out.println(subasta.toString());
            }

            if (sub.getIdEnvio() == en.getIdEnvio()) {
                ArrayList<Envio> lista2 = new ArrayList();
                lista2 = daoenvio.listarTodo();

                for (Envio envio : lista2) {
                    System.out.println(envio.toString());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(subastaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        ArrayList<Subasta> listaSubasta = new ArrayList<Subasta>();
        try {

            DAOSubasta daosub = new DAOSubasta();
            DAOEnvio daoenvio = new DAOEnvio();
            DAOTransportista daotr = new DAOTransportista();

            Subasta sub = new Subasta();
            Envio en = new Envio();
            Transportista tr = new Transportista();

            listaSubasta = daosub.listarTodo();
            
            int precio = Integer.parseInt(request.getParameter("precio"));
            String descripcion = request.getParameter("descripcion");
            String tipo_envio = request.getParameter("tipo_envio");

            //tr = daotr.buscarTransp("12458612")
            en.setTipoEnvio(tipo_envio);
            en.setIdTransp(tr.getIdTransp());

            int idEnvio = daoenvio.crear(en, tr);

            sub.setPrecio(precio);
            sub.setDescTransporte(descripcion);
            sub.setdEnvio(idEnvio);
            daosub.crear(sub, en);

            
            session.setAttribute("listaSubasta", listaSubasta );
            response.sendRedirect("index_transp.jsp");

        } catch (Exception ex) {
            Logger.getLogger(subastaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
