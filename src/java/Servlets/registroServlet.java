/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import DAOS.DAOCliente;
import DAOS.DAOProductor;
import DAOS.DAOUsuario;
import Entidades.Cliente;
import Entidades.Productor;
import Entidades.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import oracle.sql.TIMESTAMP;

/**
 *
 * @author tamar
 */
@WebServlet(name = "registro", urlPatterns = {"/registro"})
public class registroServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        PrintWriter out = response.getWriter();
        response.setContentType("text/html;charset=UTF-8");
        String rut = request.getParameter("rut");
        String nombre = request.getParameter("nombre");
        String apaterno = request.getParameter("apaterno");
        String amaterno = request.getParameter("amaterno");
        String fecha_nac = "02/05/1999"; //request.getParameter("25/25");
        String correo = request.getParameter("correo");
        String tipoUser = request.getParameter("tipoUser");
        String contrasena = request.getParameter("contrasena");
        String contrasena2 = request.getParameter("contrasena2");
        //      IUsuario usuarioDAO = null;
        DAOUsuario daou = new DAOUsuario();
        DAOCliente daocli = new DAOCliente();
        DAOProductor daopr = new DAOProductor();

        Usuario user = new Usuario();
        Cliente cli = new Cliente();
        Productor pr = new Productor();
       Date date;
        try {
            date = new SimpleDateFormat("dd/MM/yyyy").parse("31/12/1998");
            user.setRut(rut);
            user.setNombre(nombre);
            user.setApellidoPaterno(apaterno);
            user.setApellidoMaterno(amaterno);
            user.setFechaNac(date);
            user.setCorreo(correo);
            user.setContrasena(contrasena);
            user.setTipoUsuario(tipoUser);
            
            //response.getWriter().write(request.getParameter("tipoUser"));
            if (user.getTipoUsuario().equalsIgnoreCase("cliente")) {
                cli.setTipoCliente("Externo");
                cli.setRut(rut);
                daocli.crear(cli, user);
            }else{
                pr.setRut(rut);
                daopr.crear(user, pr);
            }
            

            response.sendRedirect("index.jsp");

        } catch (ParseException ex) {
            out.println("<script src='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.4/sweetalert2.all.js'></script>");
            out.println("<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>");
            out.println("<script>");
            out.println("$(document).ready(function(){");
            out.println("swal('No se pudo iniciar sesion', 'Usuario o clave incorrecto', 'error');");
            out.println("});");
            out.println("</script>");
            RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
            rd.include(request, response);
        } catch (Exception ex) {
            Logger.getLogger(registroServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
