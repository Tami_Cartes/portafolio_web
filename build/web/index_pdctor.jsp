<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Maipo Grande</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>


        <!-- Google fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;400;700&amp;display=swap">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Martel+Sans:wght@300;400;800&amp;display=swap">
        <!-- theme stylesheet-->

        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
    <body>
        <style>
            body {
                background-color: #ffdd90;
                background-image: url("img/verduras.jpeg");
            }
        </style>
        <div class="page-holder">
            <!-- navbar-->
            <header class="header bg-white">
                <div class="container px-0 px-lg-3">
                    <nav class="navbar navbar-expand-lg navbar-light py-3 px-lg-0"><a class="navbar-brand" href="index.html"><span class="font-weight-bold text-uppercase text-dark">Maipo Grande</span></a>
                        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <!-- Link--><a class="nav-link" href=""></a>
                                </li>
                                <li class="nav-item">
                                    <!-- Link--><a class="nav-link active" href="index_pdctor.jsp">Productos</a>
                                </li>
                                <li class="nav-item">
                                    <!-- Link--><a class="nav-link" href="agregarSubasta.jsp">Subasta</a>
                                </li>
                                <li class="nav-item">
                                    <!-- Link--><a class="nav-link" href="agregarPdcto.jsp">Agregar Productos</a>
                                </li>
                            </ul> 
                        </div>
                    </nav>
                </div>
            </header>
            <!--  Modal -->

            <div class="container">
                <!-- HERO SECTION-->
                <br>
                <div class="card" style="width: 21%;">
                    <div class="card-body">
                        <h5 class="card-title"><h1>Productos</h1></h5>
                    </div>
                </div>
                <section class="py-5">
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-lg-9 order-1 order-lg-2 mb-5 mb-lg-0">
                                <div class="row">


                                    <c:forEach items="${listaProductos}" var="producto">

                                        <div class="col-lg-4 col-sm-6">
                                            <div class="product text-center">
                                                <div class="mb-3 position-relative">
                                                    <div class="badge text-white badge-"></div>
                                                    <div class="product-overlay">
                                                        <ul class="mb-0 list-inline">
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="card" style="width: 18rem;">

                                                    <div class="card-body">
                                                        <h5 class="card-title">${producto.nombre}</h5>
                                                        <p class="card-text">$ ${producto.precio}</p> 
                                                        <p class="card-text">${producto.descripcion}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>&nbsp;&nbsp;&nbsp;
                                    </c:forEach>
                                    <!-- PRODUCT-->

                                </div>
                                <!-- PAGINATION-->

                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <footer class="bg-dark text-white">

            </footer>
            <!-- JavaScript files-->

            <!-- Nouislider Config-->
            <script>
                var range = document.getElementById('range');
                noUiSlider.create(range, {
                    range: {
                        'min': 0,
                        'max': 2000
                    },
                    step: 5,
                    start: [100, 1000],
                    margin: 300,
                    connect: true,
                    direction: 'ltr',
                    orientation: 'horizontal',
                    behaviour: 'tap-drag',
                    tooltips: true,
                    format: {
                        to: function (value) {
                            return '$' + value;
                        },
                        from: function (value) {
                            return value.replace('', '');
                        }
                    }
                });

            </script>
            <script>
                // ------------------------------------------------------- //
                //   Inject SVG Sprite - 
                //   see more here 
                //   https://css-tricks.com/ajaxing-svg-sprite/
                // ------------------------------------------------------ //
                function injectSvgSprite(path) {

                    var ajax = new XMLHttpRequest();
                    ajax.open("GET", path, true);
                    ajax.send();
                    ajax.onload = function (e) {
                        var div = document.createElement("div");
                        div.className = 'd-none';
                        div.innerHTML = ajax.responseText;
                        document.body.insertBefore(div, document.body.childNodes[0]);
                    }
                }
                // this is set to BootstrapTemple website as you cannot 
                // inject local SVG sprite (using only 'icons/orion-svg-sprite.svg' path)
                // while using file:// protocol
                // pls don't forget to change to your domain :)
                injectSvgSprite('https://bootstraptemple.com/files/icons/orion-svg-sprite.svg');

            </script>
            <!-- FontAwesome CSS - loading as last, so it doesn't block rendering-->
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        </div>
    </body>
</html>