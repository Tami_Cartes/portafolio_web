<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Maipo Grande</title>

  <!-- Custom fonts for this template-->
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css">
  <!-- Lightbox-->
  <link rel="stylesheet" href="/vendor/lightbox2/css/lightbox.min.css">
  <!-- Range slider-->
  <link rel="stylesheet" href="/vendor/nouislider/nouislider.min.css">
  <!-- Bootstrap select-->
  <link rel="stylesheet" href="/vendor/bootstrap-select/css/bootstrap-select.min.css">
  <!-- Owl Carousel-->
  <link rel="stylesheet" href="/vendor/owl.carousel2/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="/vendor/owl.carousel2/assets/owl.theme.default.css">
  <!-- Google fonts-->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;400;700&amp;display=swap">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Martel+Sans:wght@300;400;800&amp;display=swap">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="/css/style.default.css" id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="/css/custom.css">
  <!-- Favicon-->
  <link rel="shortcut icon" href="/img/favicon.png">

</head>

<body class="bg-gradient-primary">
<style>
            body {
  background-color: #ffdd90;
  background-image: url("img/verduras.jpeg");
}
        </style>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row justify-content-center">
              <div class="col-md-11">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Regístrate</h1>
                  </div>
                    <form class="user" method="POST" action="registro">
                    <strong><p>Ingrese sus datos</p></strong>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Rut</label>
                        <input type="text" class="form-control form-control-user" name="rut" id="rut" placeholder="12345678-9" required>
                      </div>
                      <div>
                        <label class="col-xs-6">Fecha de nacimiento</label>
                          <div id="date">
                          <input type="date" id="fecha" name="fecha" class="form-control" value="" min="1920-12-31" max="2099-12-31" required>
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12">
                        <label>Nombre</label>
                        <input type="text" class="form-control form-control-user" name="nombre" id="txtNombre" required>
                      </div>
                      <div class="col-xs-12">
                        <label>Apellido Paterno</label>
                        <input type="text" class="form-control form-control-user" name="apaterno" id="txtApaterno" required>
                      </div>
                      <div class="col-xs-12">
                        <label>Apellido Materno</label>
                        <input type="text" class="form-control form-control-user" name="amaterno" id="txtAmaterno" required>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Correo electrónico</label>
                      <input type="email" class="form-control form-control-user" name="correo" id="correo" required>
                    </div>
                    <div class="form-group">
                      <label>Ud es..?</label>
                      <select class="form-control" id="slctTipoUser" name="tipoUser" required>
                        <option value="0">Seleccione..</option>
                        <option value="productor">Productor</option>
                        <option value="cliente">Cliente</option>
                      </select>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-6 mb-3 mb-sm-0">
                        <label>Contraseña</label>
                        <input type="password" class="form-control form-control-user" name="contrasena" id="contrasena" required>
                      </div>
                      <div class="col-sm-6">
                        <label>Repita Contraseña</label>
                        <input type="password" class="form-control form-control-user" name="contrasena2" id="rptContrtasena" required>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      Registrarme
                    </button>
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="forgot-password.jsp">Olvidé mi contraseña</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="index.jsp">Ya tengo cuenta</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="/vendor/jquery/jquery.min.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="/js/sb-admin-2.min.js"></script>

</body>

</html>
