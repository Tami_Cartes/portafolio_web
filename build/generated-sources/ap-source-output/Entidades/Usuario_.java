package Entidades;

import Entidades.Administrador;
import Entidades.Cliente;
import Entidades.Productor;
import Entidades.Transportista;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-12-09T20:53:47")
@StaticMetamodel(Usuario.class)
public class Usuario_ { 

    public static volatile SingularAttribute<Usuario, String> rut;
    public static volatile SingularAttribute<Usuario, Date> fechaNac;
    public static volatile SingularAttribute<Usuario, String> apellidoPaterno;
    public static volatile CollectionAttribute<Usuario, Transportista> transportistaCollection;
    public static volatile CollectionAttribute<Usuario, Productor> productorCollection;
    public static volatile CollectionAttribute<Usuario, Administrador> administradorCollection;
    public static volatile SingularAttribute<Usuario, String> correo;
    public static volatile SingularAttribute<Usuario, String> contrasena;
    public static volatile SingularAttribute<Usuario, String> tipoUsuario;
    public static volatile SingularAttribute<Usuario, String> nombre;
    public static volatile SingularAttribute<Usuario, String> apellidoMaterno;
    public static volatile CollectionAttribute<Usuario, Cliente> clienteCollection;

}