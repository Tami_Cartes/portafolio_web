

CREATE TABLE administrador (
    usuario_rut   VARCHAR2(10) NOT NULL,
    id_admin      NUMBER(8) NOT NULL 
);

ALTER TABLE administrador ADD CONSTRAINT administrador_pk PRIMARY KEY ( id_admin );

CREATE TABLE boleta (
    producto_id_producto     NUMBER(6) NOT NULL,
    id_boleta                NUMBER(6) NOT NULL,
    detalle_venta            VARCHAR2(100) NOT NULL,
    administrador_id_admin   NUMBER(8) NOT NULL,
    venta_id_venta           NUMBER(8)
);

ALTER TABLE boleta ADD CONSTRAINT boleta_pk PRIMARY KEY ( id_boleta );

CREATE TABLE cliente (
    usuario_rut    VARCHAR2(10) NOT NULL,
    id_cliente     NUMBER(8) NOT NULL,
    tipo_cliente   VARCHAR2(30) NOT NULL
);

ALTER TABLE cliente ADD CONSTRAINT cliente_pk PRIMARY KEY ( id_cliente );

CREATE TABLE consultor (
    usuario_rut    VARCHAR2(10) NOT NULL,
    id_consultor   NUMBER(8) NOT NULL
);

ALTER TABLE consultor ADD CONSTRAINT consultor_pk PRIMARY KEY ( id_consultor );

CREATE TABLE contrato (
    id_contrato              NUMBER(8) NOT NULL,
    descripcion              VARCHAR2(100) NOT NULL,
    productor_id_productor   NUMBER(8) NOT NULL
);

ALTER TABLE contrato ADD CONSTRAINT contrato_pk PRIMARY KEY ( id_contrato );

CREATE TABLE envio (
    id_envio                  NUMBER(6) NOT NULL,
    tipo_envio                VARCHAR2(20) NOT NULL,
    transportista_id_transp   NUMBER(8) NOT NULL
);

ALTER TABLE envio ADD CONSTRAINT envio_pk PRIMARY KEY ( id_envio );

CREATE TABLE pago (
    boleta_id_boleta   NUMBER(6) NOT NULL,
    id_pago            NUMBER(8) NOT NULL,
    tipo_pago          VARCHAR2(100) NOT NULL
);

ALTER TABLE pago ADD CONSTRAINT pago_pk PRIMARY KEY ( id_pago );

CREATE TABLE producto (
    id_producto              NUMBER(6) NOT NULL,
    precio                   NUMBER(7) NOT NULL,
    cantidad                 NUMBER(3) NOT NULL,
    descripcion              VARCHAR2(100) NOT NULL,
    productor_id_productor   NUMBER(8) NOT NULL
);

ALTER TABLE producto ADD CONSTRAINT producto_pk PRIMARY KEY ( id_producto );

CREATE TABLE productor (
    usuario_rut    VARCHAR2(10) NOT NULL,
    id_productor   NUMBER(8) NOT NULL
);

ALTER TABLE productor ADD CONSTRAINT productor_pk PRIMARY KEY ( id_productor );

CREATE TABLE resumen_venta (
    id_resumen         NUMBER(8) NOT NULL,
    costo_transporte   NUMBER(8) NOT NULL,
    impuestos          NUMBER(8) NOT NULL,
    pago_servicio      NUMBER(8) NOT NULL,
    comision_empresa   NUMBER(8) NOT NULL,
    venta_id_venta     NUMBER(8) NOT NULL
);

ALTER TABLE resumen_venta ADD CONSTRAINT resumen_venta_pk PRIMARY KEY ( id_resumen );

CREATE TABLE solicitud_compra (
    id_solicitud         NUMBER(6) NOT NULL,
    cliente_id_cliente   NUMBER(8) NOT NULL
);

ALTER TABLE solicitud_compra ADD CONSTRAINT solicitud_compra_pk PRIMARY KEY ( id_solicitud );

CREATE TABLE subasta (
    id_subasta        NUMBER(6) NOT NULL,
    precio            NUMBER(8) NOT NULL,
    envio_id_envio    NUMBER(6) NOT NULL,
    desc_transporte   VARCHAR2(100) NOT NULL
);

ALTER TABLE subasta ADD CONSTRAINT subasta_pk PRIMARY KEY ( id_subasta );

CREATE TABLE transportista (
    usuario_rut   VARCHAR2(10) NOT NULL,
    id_transp     NUMBER(8) NOT NULL
);

ALTER TABLE transportista ADD CONSTRAINT transportista_pk PRIMARY KEY ( id_transp );

CREATE TABLE usuario (
    rut                VARCHAR2(10) NOT NULL,
    nombre             VARCHAR2(50) NOT NULL,
    apellido_paterno   VARCHAR2(50) NOT NULL,
    apellido_materno   VARCHAR2(50) NOT NULL,
    fecha_nac          DATE NOT NULL,
    correo             VARCHAR2(50),
    contrasena         VARCHAR2(8) NOT NULL,
    tipo_usuario       VARCHAR2(45) NOT NULL
);

ALTER TABLE usuario ADD CONSTRAINT usuario_pk PRIMARY KEY ( rut );

CREATE TABLE venta (
    id_venta     NUMBER(8) NOT NULL,
    tipo_venta   VARCHAR2(50) NOT NULL
);

ALTER TABLE venta ADD CONSTRAINT venta_pk PRIMARY KEY ( id_venta );

ALTER TABLE administrador
    ADD CONSTRAINT administrador_usuario_fk FOREIGN KEY ( usuario_rut )
        REFERENCES usuario ( rut );

ALTER TABLE boleta
    ADD CONSTRAINT boleta_administrador_fk FOREIGN KEY ( administrador_id_admin )
        REFERENCES administrador ( id_admin );

ALTER TABLE boleta
    ADD CONSTRAINT boleta_producto_fk FOREIGN KEY ( producto_id_producto )
        REFERENCES producto ( id_producto );

ALTER TABLE boleta
    ADD CONSTRAINT boleta_venta_fk FOREIGN KEY ( venta_id_venta )
        REFERENCES venta ( id_venta );

ALTER TABLE cliente
    ADD CONSTRAINT cliente_usuario_fk FOREIGN KEY ( usuario_rut )
        REFERENCES usuario ( rut );

ALTER TABLE constultor
    ADD CONSTRAINT constultor_usuario_fk FOREIGN KEY ( usuario_rut )
        REFERENCES usuario ( rut );

ALTER TABLE contrato
    ADD CONSTRAINT contrato_productor_fk FOREIGN KEY ( productor_id_productor )
        REFERENCES productor ( id_productor );

ALTER TABLE envio
    ADD CONSTRAINT envio_transportista_fk FOREIGN KEY ( transportista_id_transp )
        REFERENCES transportista ( id_transp );

ALTER TABLE pago
    ADD CONSTRAINT pago_boleta_fk FOREIGN KEY ( boleta_id_boleta )
        REFERENCES boleta ( id_boleta );

ALTER TABLE producto
    ADD CONSTRAINT producto_productor_fk FOREIGN KEY ( productor_id_productor )
        REFERENCES productor ( id_productor );

ALTER TABLE productor
    ADD CONSTRAINT productor_usuario_fk FOREIGN KEY ( usuario_rut )
        REFERENCES usuario ( rut );

ALTER TABLE resumen_venta
    ADD CONSTRAINT resumen_venta_venta_fk FOREIGN KEY ( venta_id_venta )
        REFERENCES venta ( id_venta );

ALTER TABLE solicitud_compra
    ADD CONSTRAINT solicitud_compra_cliente_fk FOREIGN KEY ( cliente_id_cliente )
        REFERENCES cliente ( id_cliente );

ALTER TABLE subasta
    ADD CONSTRAINT subasta_envio_fk FOREIGN KEY ( envio_id_envio )
        REFERENCES envio ( id_envio );

ALTER TABLE transportista
    ADD CONSTRAINT transportista_usuario_fk FOREIGN KEY ( usuario_rut )
        REFERENCES usuario ( rut );


--SECUENCIAS
CREATE SEQUENCE Cliente_seq START WITH 1 INCREMENT BY 1;

CREATE OR REPLACE TRIGGER T_cliente_ID
 BEFORE INSERT ON Cliente FOR EACH ROW
 WHEN (NEW.id_cliente IS NULL)
BEGIN
 SELECT Cliente_seq.NEXTVAL INTO :NEW.id_cliente FROM DUAL;
END;
/

CREATE OR REPLACE TRIGGER T_productor_ID
BEFORE INSERT
ON productor
REFERENCING NEW AS NEW
FOR EACH ROW
BEGIN
     if(:new.id_productor is null) then
     SELECT S_id.nextval
     INTO :new.id_productor
     FROM dual;
     end if;
END;
/

ALTER TRIGGER T_cliente_ID ENABLE;
ALTER TRIGGER T_productor_ID ENABLE;
commit;
