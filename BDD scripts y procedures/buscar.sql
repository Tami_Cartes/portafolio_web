create or REPLACE procedure buscar_usuario(
    p_rut varchar2,
    v_nombre out usuario.nombre%type,
    v_apaterno out usuario.apellido_paterno%type,
    v_amaterno out usuario.apellido_materno%type,
    v_fecha_nac out usuario.fecha_nac%type,
    v_correo out  usuario.correo%type,
    v_contrasena out usuario.contrasena%type ,
    v_tipo_usuario out usuario.tipo_usuario%type
) is
    begin 
        select nombre,apellido_paterno, apellido_materno, fecha_nac, correo, contrasena, tipo_usuario 
        into v_nombre, v_apaterno, v_amaterno, v_fecha_nac, v_correo, v_contrasena, v_tipo_usuario
        from usuario where rut = p_rut;
        commit;
end buscar_usuario;
/

create or REPLACE procedure buscar_admin(
    p_rut varchar2,
    v_id_admin out administrador.id_admin%type, 
    v_nombre out usuario.nombre%type,
    v_apaterno out usuario.apellido_paterno%type,
    v_amaterno out usuario.apellido_materno%type,
    v_fecha_nac out usuario.fecha_nac%type,
    v_correo out  usuario.correo%type
) is
    begin 
        select a.id_admin, u.nombre, u.apellido_paterno, u.apellido_materno, u.fecha_nac, u.correo
        into v_id_admin, v_nombre, v_apaterno, v_amaterno, v_fecha_nac, v_correo
        from administrador a join usuario u on u.rut = a.usuario_rut where a.usuario_rut = p_rut order by a.id_admin;
        commit;
end buscar_admin;
/

create or REPLACE procedure buscar_boleta(
    p_id_boleta int,
    v_db out boleta.detalle_venta%type, 
    v_precio out producto.precio%type,
    v_cantidad out producto.cantidad%type,
    v_descripcion out producto.descripcion%type,
    v_tipo_venta out venta.tipo_venta%type    
) is
    begin 
        select b.detalle_venta, p.precio, p.cantidad, p.descripcion,v.tipo_venta
        into v_db, v_precio, v_cantidad, v_descripcion, v_tipo_venta
        from boleta b join producto p on p.id_producto = b.producto_id_producto
        join venta v on v.id_venta = b.venta_id_venta;
        commit;
end buscar_boleta;
/

create or REPLACE PROCEDURE buscar_transp(
    p_rut varchar2,
    v_idTransp out transportista.id_transp%type
    
) is
begin
    select id_transp
    into v_idTransp
    from Transportista where usuario_rut = p_rut;
    commit;
end buscar_transp;
/

create or REPLACE PROCEDURE buscar_envio(
    p_idTransp varchar2,
    v_idEnvio out envio.id_envio%type
    
) is
begin
    select id_envio
    into v_idEnvio
    from envio where transportista_id_transp = p_idTransp;
    commit;
end buscar_envio;
/
