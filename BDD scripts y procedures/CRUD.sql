drop PROCEDURE insert_usuario;
drop PROCEDURE delete_usuario;
drop procedure update_usuario;
drop PROCEDURE insert_admin;
drop PROCEDURE delete_admin;
drop procedure insert_boleta;
drop PROCEDURE delete_boleta;
drop PROCEDURE update_boleta;
drop PROCEDURE insert_cliente;
drop PROCEDURE delete_cliente;
drop PROCEDURE update_cliente;
drop PROCEDURE insert_consultor;
drop PROCEDURE delete_consultor;
drop PROCEDURE insert_contrato;
drop PROCEDURE delete_contrato;
drop PROCEDURE update_contrato;
drop PROCEDURE insert_envio;
drop PROCEDURE delete_envio;
drop PROCEDURE update_envio;
drop PROCEDURE insert_pago;
drop PROCEDURE delete_pago;
drop PROCEDURE update_pago;
drop PROCEDURE insert_producto;
drop PROCEDURE delete_producto;
drop PROCEDURE update_producto;
drop PROCEDURE insert_pdctor;
drop PROCEDURE delete_pdctor;
drop PROCEDURE insert_resumen;
drop PROCEDURE delete_resumen;
drop PROCEDURE update_resumen;
drop PROCEDURE insert_solicitud;
drop PROCEDURE delete_solicitud;
drop PROCEDURE insert_subasta;
drop PROCEDURE delete_subasta;
drop PROCEDURE update_subasta;
drop PROCEDURE insert_trans;
drop PROCEDURE delete_trans;
drop PROCEDURE insert_venta;
drop PROCEDURE delete_venta;
drop PROCEDURE update_venta;


 

-----------
--USUARIO--
-----------

create or replace PROCEDURE INSERT_USUARIO (P_RUT VARCHAR2, P_NOMBRE varchar2,P_APATERNO VARCHAR2,
                                            P_AMATERNO VARCHAR2,P_FECHA_NAC date,P_CORREO VARCHAR2,
                                            P_CONTRASENA VARCHAR2, P_TIPO_USUARIO varchar2, v_usuario out usuario.rut%TYPE)
	IS
		BEGIN
		INSERT INTO USUARIO(RUT, NOMBRE,apellido_paterno, apellido_materno,FECHA_NAC ,CORREO,CONTRASENA,TIPO_USUARIO) 
        VALUES (P_RUT,P_NOMBRE,p_apaterno, p_amaterno, p_fecha_nac ,p_correo,P_CONTRASENA, p_tipo_usuario) returning rut into V_USUARIO ;
		commit;
END INSERT_USUARIO;
/

create or replace PROCEDURE UPDATE_USUARIO (P_RUT VARCHAR2, P_NOMBRE varchar2,P_APATERNO VARCHAR2,
                                            P_AMATERNO VARCHAR2,P_FECHA_NAC date,P_CORREO VARCHAR2,
                                            P_CONTRASENA VARCHAR2, P_TIPO_USUARIO varchar2)
	IS
		BEGIN
		UPDATE USUARIO SET  nombre           = P_NOMBRE,
                            apellido_paterno = P_apaterno,
		  	                apellido_materno = P_AMATERNO,
		         	        fecha_nac        = P_FECHA_NAC,
                            correo           = P_CORREO,
		           		  	contrasena       = P_CONTRASENA,
                            tipo_usuario     = p_tipo_usuario
		        		   	WHERE P_RUT      = rut;

		COMMIT;
END UPDATE_USUARIO;
/

create or replace procedure DELETE_USUARIO(P_RUT number)
	IS
	BEGIN
	   DELETE FROM USUARIO WHERE rut=P_RUT;
	commit;
END DELETE_USUARIO;
/

Create or replace procedure listar_usuario(c_listaUser out sys_refcursor)
as
begin
open c_listaUser for
	 select rut, nombre, apellido_paterno, apellido_materno,fecha_nac,correo,contrasena,tipo_usuario
	 from usuario order by rut asc;
	 commit;
end listar_usuario;
/

-----------------
--Administrador--
-----------------
create or replace procedure insert_admin(p_idAdmin number, v_admin out administrador.id_admin%TYPE)
is
Begin
    insert into administrador(id_admin) values(P_idAdmin) returning id_admin into v_admin;
    commit;
end insert_admin;
/

create or replace procedure delete_admin(p_idAdmin number)
is
Begin
    delete from administrador where id_admin = p_idAdmin;
    commit;
end delete_admin;
/

create or replace procedure listar_admin(c_listarAdmin out sys_refcursor)
as
begin
open c_listarAdmin for
    select a.id_admin, a.usuario_rut, u.nombre, u.apellido_paterno, u.apellido_materno, u.fecha_nac, u.correo
    from administrador a join usuario u on u.rut = a.usuario_rut order by a.id_admin;
    commit;
end listar_admin;
/
------------
-- CLIENTE--
------------
create or replace procedure insert_cliente(P_tipo_cliente varchar2, p_usuario_rut varchar2,v_cliente out cliente.id_cliente%TYPE)
is
Begin
    insert into cliente(tipo_cliente,usuario_rut) values(p_tipo_cliente,p_usuario_rut) returning id_cliente into v_cliente;
    commit;
end insert_cliente;
/

create or replace PROCEDURE UPDATE_cliente (P_idCliente number, p_tipo_cliente varchar2)
	IS
		BEGIN
		UPDATE cliente SET tipo_cliente = p_tipo_Cliente
                      WHERE P_idCliente = id_cliente;

		COMMIT;
END UPDATE_cliente;
/

create or replace procedure delete_cliente(p_idCliente number)
is
begin
    delete from cliente where id_cliente = p_idCliente;
    commit;
end delete_cliente;
/

create or replace procedure listar_cliente(c_listarCliente out sys_refcursor)
as
begin
open c_listarCliente for
    select  c.id_cliente, c.usuario_rut, u.nombre,u.apellido_paterno,u.apellido_materno,u.fecha_nac,u.correo, c.tipo_cliente 
    from cliente c join usuario u on u.rut = c.usuario_rut order by c.id_cliente;
    commit;
end listar_cliente;
/
-------------
--CONSULTOR--
--------------
create or replace procedure insert_consultor(p_idConsultor number,p_usuario_rut varchar, v_consultor out consultor.id_consultor%TYPE)
is
Begin
    insert into consultor(id_consultor,usuario_rut) values(P_idConsultor,p_usuario_rut) returning id_consultor into v_consultor;
    commit;
end insert_consultor;
/

create or replace procedure delete_consultor(p_idConsultor number)
is
begin
    delete from consultor where id_consultor = p_idConsultor;
    commit;
end delete_consultor;
/

create or replace procedure listar_consultor(c_listarConsultor out sys_refcursor)
as
begin
open c_listarConsultor for
    select  con.id_consultor,con.usuario_rut,u.nombre,u.apellido_paterno,u.apellido_materno,u.fecha_nac,u.correo
    from consultor con join usuario u on u.rut = con.usuario_rut order by con.id_consultor;
    commit;
end listar_consultor;
/
-------------
--PRODUCTOR--
--------------
create or replace procedure insert_pdctor(p_idPdctor number,p_usuario_rut varchar2, v_pdctor out productor.id_productor%TYPE)
is
Begin
    insert into productor(id_productor, usuario_rut) values(P_idPdctor, p_usuario_rut) returning id_productor into v_pdctor;
    commit;
end insert_pdctor;
/

create or replace procedure delete_pdctor(p_idPdctor number)
is
begin
    delete from productor where id_productor = p_idPdctor;
    commit;
end delete_pdctor
;
/

create or replace procedure listar_pdctor(c_listarPdctor out sys_refcursor)
as
begin
open c_listarPdctor for
    select  p.id_productor,p.usuario_rut,u.nombre,u.apellido_paterno,u.apellido_materno,u.fecha_nac,u.correo
    from productor p join usuario u on u.rut = p.usuario_rut order by p.id_productor;
    commit;
end listar_pdctor;
/
-----------------
--TRANSPORTISTA--
-----------------

create or replace procedure insert_trans(p_idTrans number,p_usuario_rut numebr, v_trans out transportista.id_transp%TYPE)
is
Begin
    insert into transportista(id_transp, usuario_rut) values(P_idTrans,p_usuario_rut) returning id_transp into v_trans;
    commit;
end insert_trans;
/

create or replace procedure delete_trans(p_idTrans number)
is
begin
    delete from transportista where id_transp = p_idTrans;
    commit;
end delete_trans;
/

create or replace procedure listar_transp(c_listarTransp out sys_refcursor)
as
begin
open c_listarTransp for
    select t.id_transp,t.usuario_rut, e.tipo_envio
    from transportista t join usuario u on u.rut = t.usuario_rut 
    join envio e on t.id_transp = e.transportista_id_transp order by t.id_transp;
    commit;
end listar_transp;
/

--------------------
--SOLICITUD COMPRA--
--------------------
create or replace procedure insert_solicitud(p_idSolicitud number, v_solicitud out solicitud_compra.id_solicitud%TYPE)
is
Begin
    insert into solicitud_compra(id_solicitud) values(P_idSolicitud) returning id_solicitud into v_solicitud;
    commit;
end insert_solicitud;
/

create or replace procedure delete_solicitud(p_idSolicitud number)
is
begin
    delete from solicitud_compra where id_solicitud = p_idSolicitud;
    commit;
end delete_solicitud;
/

create or replace procedure listar_solicitud(c_listarSolicitud out sys_refcursor)
as
begin
open c_listarSolicitud for
    select s.id_solicitud,s.cliente_id_cliente
    from solicitud_compra s join cliente c on c.id_cliente = s.cliente_id_cliente order by s.id_solicitud;
    commit;
end listar_solicitud;
/
---------
--VENTA--
---------
create or replace procedure insert_venta(p_idVenta number, p_tipo_venta varchar2, v_venta out venta.id_venta%TYPE)
is
Begin
    insert into venta(id_venta,tipo_venta) values(P_idVenta, p_tipo_venta) returning id_venta into v_venta;
    commit;
end insert_venta;
/

create or replace procedure update_venta(p_idVenta number, p_tipo_venta varchar2)
is
begin
    update venta set tipo_venta = p_tipo_venta
                 where p_idVenta = id_venta;
    commit;
end update_venta ;
/

create or replace procedure delete_venta(P_idVenta number)
is
begin
    delete from venta where id_venta = p_idVenta;
    commit;
end delete_venta;
/

create or replace procedure listar_venta(c_listarVenta out sys_refcursor)
as
begin
open c_listarVenta for
    select id_venta,tipo_venta
    from venta order by id_venta;
    commit;
end listar_venta;
/
----------
--BOLETA--
----------
create or replace PROCEDURE INSERT_BOLETA (P_idBoleta number, P_detVenta varchar2, v_detVenta out boleta.id_boleta%type)
	IS
		BEGIN
		INSERT INTO boleta(id_boleta,detalle_venta) 
        VALUES (P_idBoleta, p_detVenta) returning id_boleta into V_detVenta ;
		commit;
END INSERT_boleta;
/

create or replace PROCEDURE UPDATE_boleta (P_idBoleta number, P_detVenta varchar2)
	IS
		BEGIN
		UPDATE boleta SET   detalle_venta   = P_detVenta
		        		   	WHERE P_idBoleta = id_boleta;

		COMMIT;
END UPDATE_boleta;
/

create or replace procedure DELETE_boleta(P_idBoleta number)
	IS
	BEGIN
	   DELETE FROM boleta WHERE id_boleta=P_idBoleta;
	commit;
END DELETE_boleta;
/

create or replace procedure listar_boleta(c_listarBoleta out sys_refcursor)
as
begin
open c_listarBoleta for
    select b.id_boleta, b.detalle_venta, p.precio, p.cantidad, p.descripcion,v.tipo_venta
    from boleta b join producto p on p.id_producto = b.producto_id_producto 
    join administrador a on a.id_admin = b.administrador_id_admin
    join venta v on v.id_venta = b.venta_id_venta;
    commit;
end listar_boleta;
/

------------
--CONTRATO--
------------
create or replace PROCEDURE INSERT_contrato (P_idContrato number, P_descripcion varchar2,p_pdctor_id number, v_contrato out contrato.id_contrato%type)
	IS
		BEGIN
		INSERT INTO contrato(id_contrato,descripcion,productor_id_productor) 
        VALUES (P_idContrato, p_descripcion,p_pdctor_id) returning id_contrato into V_contrato;
		commit;
END INSERT_contrato;
/

create or replace PROCEDURE UPDATE_contrato (P_idContrato number, P_descripcion varchar2)
	IS
		BEGIN
		UPDATE contrato SET   descripcion   = P_descripcion
		        		   	WHERE P_idContrato = id_contrato;

		COMMIT;
END UPDATE_contrato;
/

create or replace procedure DELETE_contrato(P_idContrato number)
	IS
	BEGIN
	   DELETE FROM contrato WHERE id_contrato=P_idContrato;
	commit;
END DELETE_contrato;
/

create or replace procedure listar_contrato(c_listarContrato out sys_refcursor)
as
begin
open c_listarContrato for
    select c.id_contrato, c.descripcion, p.usuario_rut, u.nombre, u.apellido_paterno, u.apellido_materno, u.fecha_nac
    from contrato c join productor p on p.id_productor = c.productor_id_productor
    join usuario u on u.rut = p.usuario_rut;
    commit;
end listar_contrato;
/

---------
--ENVIO--
---------
create or replace PROCEDURE insert_envio (P_idEnvio number, P_tipo_envio varchar2,P_transp_id number, v_envio out envio.id_envio%type)
	IS
		BEGIN
		INSERT INTO envio(id_envio,tipo_envio, transportista_id_transp) 
        VALUES (P_idEnvio, p_tipo_envio,p_transp_id) returning id_envio into V_envio ;
		commit;
END INSERT_envio;
/

create or replace PROCEDURE UPDATE_envio (P_idEnvio number, P_tipo_envio varchar2)
	IS
		BEGIN
		UPDATE envio SET tipo_envio   = P_tipo_envio
		        		   	WHERE P_idEnvio = id_envio;

		COMMIT;
END UPDATE_envio;
/

create or replace procedure DELETE_envio(P_idEnvio number)
	IS
	BEGIN
	   DELETE FROM envio WHERE id_envio = P_idEnvio;
	commit;
END DELETE_envio;
/

create or replace procedure listar_envio(c_listarEnvio out sys_refcursor)
as
begin
open c_listarEnvio for
    select id_envio, tipo_envio
    from envio order by id_envio asc;
    commit;
end listar_envio;
/

--------
--PAGO--
--------
create or replace PROCEDURE insert_pago(P_idPago number, P_tipo_pago varchar2,p_idBoleta int, v_pago out pago.id_pago%type)
	IS
		BEGIN
		INSERT INTO pago(id_pago,tipo_pago, boleta_id_boleta) 
        VALUES (P_idPago, p_tipo_pago, p_idBoleta) returning id_pago into V_pago ;
		commit;
END INSERT_pago;
/

create or replace PROCEDURE UPDATE_pago (P_idPago number, P_tipo_pago varchar2)
	IS
		BEGIN
		UPDATE pago SET tipo_pago   = P_tipo_pago
		        		   	WHERE P_idPago = id_pago;

		COMMIT;
END UPDATE_pago;
/

create or replace procedure DELETE_pago(P_idPago number)
	IS
	BEGIN
	   DELETE FROM pago WHERE id_pago = P_idPago;
	commit;
END DELETE_pago;
/

create or replace procedure listar_pago(c_listarPago out sys_refcursor)
as
begin
open c_listarPago for
    select id_pago, tipo_pago
    from pago order by id_pago asc;
    commit;
end listar_pago;
/
------------
--PRODUCTO--
------------

create or replace PROCEDURE INSERT_producto (P_idProducto number, P_precio number,P_cantidad number,
                                            P_descripcion VARCHAR2, v_producto out producto.id_producto%TYPE)
	IS
		BEGIN
		INSERT INTO producto(id_producto, precio,cantidad, descripcion) 
                    values (P_idProducto, P_precio,P_cantidad,P_descripcion)returning id_producto into V_producto ;
		commit;
END INSERT_producto;
/

create or replace PROCEDURE UPDATE_producto (P_idProducto number, P_precio number,P_cantidad number,P_descripcion VARCHAR2)
	IS
		BEGIN
		UPDATE producto SET precio      = p_precio,
                            cantidad    = p_cantidad,
                            descripcion = p_descripcion
		        		   	WHERE P_idProducto = id_producto;

		COMMIT;
END UPDATE_producto;
/

create or replace procedure DELETE_producto(P_idProducto number)
	IS
	BEGIN
	   DELETE FROM producto WHERE id_producto = P_idProducto;
	commit;
END DELETE_producto;
/

create or replace procedure listar_pdcto(c_listarPdcto out sys_refcursor)
as
begin
open c_listarPdcto for
     select id_producto, precio, cantidad,descripcion 
     from producto order by id_producto;
     commit;
end listar_pdcto;
/

-----------------
--RESUMEN VENTA--
-----------------
create or replace PROCEDURE INSERT_RESUMEN (P_idResumen number, P_costo_trans number,P_impuestos number,
                                            P_pago_servicio number,P_com_empresa number, p_venta_id number, v_resumen out resumen_venta.id_resumen%TYPE)
	IS
		BEGIN
		INSERT INTO resumen_venta(id_resumen, costo_transporte,impuestos, pago_servicio,comision_empresa, venta_id_venta) 
        VALUES (P_idResumen, P_costo_trans,P_impuestos,P_pago_servicio,P_com_empresa,p_venta_id) returning id_resumen into V_resumen ;
		commit;
END INSERT_resumen;
/

create or replace PROCEDURE UPDATE_resumen (P_idResumen number, P_costo_trans number,P_impuestos number, P_pago_servicio number,P_com_empresa number)
	IS
		BEGIN
		UPDATE resumen_venta SET costo_transporte = p_costo_trans,
								 impuestos        = p_impuestos,
								 pago_servicio    = p_pago_servicio,
								 comision_empresa = p_com_empresa
		        		   	WHERE P_idResumen= id_resumen;

		COMMIT;
END UPDATE_resumen;
/

create or replace procedure DELETE_resumen(P_idResumen number)
	IS
	BEGIN
	   DELETE FROM resumen_venta WHERE id_resumen = P_idResumen;
	commit;
END DELETE_resumen;
/

create or replace procedure listar_resumen(c_listarResumen out sys_refcursor)
as
begin
open c_listarResumen for
    select rv.id_resumen, rv.costo_transporte, rv.impuestos, rv.pago_servicio, rv.comision_empresa, v.tipo_venta
    from resumen_venta rv join venta v on v.id_venta = rv.venta_id_venta order by id_resumen;
    commit;
end listar_resumen;
/
-----------
--SUBASTA--
-----------
create or replace PROCEDURE INSERT_subasta (P_idSubasta number, P_precio number,P_descTrans varchar2,p_id_envio number, v_subasta out subasta.id_subasta%TYPE)
	IS
		BEGIN
		INSERT INTO subasta(id_subasta, precio,desc_transporte, envio_id_envio) 
                    values(P_idSubasta, P_precio,P_descTrans, p_id_envio) returning id_subasta into V_subasta ;
		commit;
END INSERT_subasta;
/

create or replace PROCEDURE UPDATE_subasta(P_idSubasta number, P_precio number,P_descTrans VARCHAR2)
	IS
		BEGIN
		UPDATE subasta SET precio      = p_precio,
                            desc_transporte = p_descTrans
		        		   	WHERE  p_idSubasta = id_subasta;

		COMMIT;
END UPDATE_subasta;
/

create or replace procedure DELETE_subasta(P_idSubasta number)
	IS
	BEGIN
	   DELETE FROM subasta WHERE id_subasta = P_idSubasta;
	commit;
END DELETE_Subasta;
/

create or replace procedure listar_subasta(c_listarSubasta out sys_refCursor)
as
begin
open c_listarSubasta for
     select s.id_subasta, s.precio, s.desc_transporte, e.tipo_envio
     from subasta s join envio e on e.id_envio = s.envio_id_envio order by s.id_subasta;
     commit;
end listar_subasta;